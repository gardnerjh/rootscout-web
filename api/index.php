<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */
 
// POST route
$app->post('/locations/route', function () use ($app) {
	$json = $app->request->getBody();
	$data = json_decode($json);
	$maxDistanceMeters = ($data->distance) * 1609.34;
		
	$m = new MongoClient('mongodb://mealscout:mealscout@ds051640.mongolab.com:51640/mealscout');
	$locationsCollection = $m->mealscout->locations;
	
	$locationArray = array();
	foreach($data->coordinates as $coordinate) {
		$query = array(	'geolocation' => array(
							'$near' => array(
								'$geometry' => array(
									'type' => 'Point', 
									'coordinates' => array($coordinate->longitude, $coordinate->latitude)
								), 
								'$maxDistance' => $maxDistanceMeters,
								'$minDistance' => 0
							)
						)
		);
		$cursor = $locationsCollection->find($query);
		foreach($cursor as $document) {
			if(!in_array($document, $locationArray)) {
				array_push($locationArray, $document);
			}
		}
	}
	
	echo json_encode($locationArray);
});

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
