var apiBaseUrl = 'http://www.jeffyg.com/rootscout/api';

var geocoder = new google.maps.Geocoder();
var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer();
var apiEndpointPath = '/locations/route';

var map;
var routes;
var selectedRouteIndex = 0;
var markers = [];
var openInfoWindow;

function initializeMap() {
	var mapCanvas = document.getElementById('map_canvas');
	var mapOptions = {
		center: new google.maps.LatLng(40.339909, -97.927915),
		zoom: 3,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
		zoomControl: true
	};
	map = new google.maps.Map(mapCanvas, mapOptions);
}

function drawDirections(originAddress, destinationAddress, callback) {
	var request = {
		origin: originAddress,
		destination: destinationAddress,
		travelMode: google.maps.TravelMode.DRIVING
	};
	directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			callback(result);
		} else {
			alert('Could not get directions from \'' + originAddress + '\' to \'' + destinationAddress + '\'<br />' + status);
		}
	});
}

function updateMap(originAddress, destinationAddress, miles) {
	// Clear out any existing markers and directions
	$(markers).each(function() {
		this.setMap(null);
	});
	directionsDisplay.setMap(null);
	directionsDisplay = new google.maps.DirectionsRenderer();
	directionsDisplay.setMap(map);
	
	drawDirections(originAddress, destinationAddress, function(result) {
		directionsDisplay.setDirections(result);
		pinLocations(result.routes[selectedRouteIndex], miles);
		centerMap();
	});
}

function pinLocations(route, miles) {
	var coordinateArray = [];
	$(route.overview_path).each(function(){
		var coordinates = { latitude: this.A, longitude: this.F };
		coordinateArray.push(coordinates);
	});

	var payload = {
		distance: miles,
		coordinates: coordinateArray
	};

	var apiEndpointUrl = apiBaseUrl + apiEndpointPath;
	var request = $.ajax({
		url: apiEndpointUrl,
		type: 'POST',
		data: JSON.stringify(payload),
		dataType: 'json'
	}).fail(function(jqXHR, textStatus){
		alert('Could not retrieve locations. Please try again later.');
	}).done(function(result){
		if (result.length > 0) {
			$(result).each(function() {
				var location = this;

				var longitude = location.geolocation.coordinates[0];
				var latitude = location.geolocation.coordinates[1];

				var latLng = new google.maps.LatLng(latitude, longitude);
				
				var infoPanelHtml = $.get('/rootscout/infoPanel.html', function(data){
					var html = data.replace("##name##", location.name).replace("##imageUrl##", location.imageUrl).replace("##streetAddress##", location.address.streetAddress).replace("##city##", location.address.city).replace("##state##", location.address.state).replace("##postalCode##", location.address.postalCode)
					var marker = addMarker(map, location.name, latLng, html);
					markers.push(marker);
				});
			});
		} else {
			alert("No locations were found near this route.");
		}
	});
}

function centerMap() {
	var bounds = map.getBounds();
	$(markers).each(function() {
		bounds.extend(this.position);
	});
	map.fitBounds(bounds);
}

function addMarker(map, title, location, infoWindowContent) {
	var marker = new google.maps.Marker({
		map: map,
		title: title,
		position: location,
		animation: google.maps.Animation.DROP
	});
	
	if (infoWindowContent != null) {
		var infoWindow = new google.maps.InfoWindow({
			content: infoWindowContent,
			maxWidth: 200
		});
		
		google.maps.event.addListener(marker, 'click', function() {
			if (openInfoWindow != null) {
				openInfoWindow.close();				
			}
			infoWindow.open(map, marker);
			openInfoWindow = infoWindow;
		});
	}
	
	return marker;
}