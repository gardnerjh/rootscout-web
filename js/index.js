$(document).ready(function() {
	prepopulateFields();
	initializeMap();

	$('#go').click(function(){
		var originAddress = $('#originAddress').val();
		var destinationAddress = $('#destinationAddress').val();
		var miles = $('#miles').val();
		updateMap(originAddress, destinationAddress, miles);
	});
});

function prepopulateFields() {
	$('#originAddress').val('145 Summit Ave. Summit NJ 07901');
	$('#destinationAddress').val('163 Madison Ave. Morristown NJ 07960');
	$('#miles').val('5');
}